#Intro
Cette application a pour vocation de créer une application similaire à "Qui est le moins cher" de leclerc mais indépendante et individuelle.

#1 Réflexions sur son fonctionnement
1. les utilisateurs créent une liste de course, de produits à acheter et l'application leur retourne le lieu le moins cher pour chaque produit
2. l'application peut lisser les résultats pour avoir moins de lieux 
    1. les produits avec le lieu le moins représenté seront mis à jour avec le premier lieu présent dans la liste en partant du moins cher
    2. voir la pertinence de gérer le lissage par proximité (si un produit est moins cher mais à 20km des autre est ce qu'il ne vaut pas mieux dépenser 1€ de plus que de faire 40km ?)
3. l'utilisateur pourra rajouter les quantités afin de calculer le montant des courses
    1. du produit acheté (genre 1 boite de thon)
    2. de l'unité du produit (genre 1kg de farine)
4. l'utilisateur pourra modifier manuellement les lieux sélectionnés par l'application
5. les produits sont classés en trois catégories : 1er prix, bon rapport qualité/prix ,de qualité
6. les produits peuvent apparaitrent dans plusieurs magasins, même s'ils ne sont pas exactement les mêmes à condition qu'il soient de même qualité. Par exemple si on considère que le fromage de chèvre acheté au rayon découpe de carrefour est de la même qualité que le fromage de chèvre du fromager du marché ils apparaitront en tant que même produit pour l'application
7. la liste des produits affichera le prix par magasin, le prix (et le lieux associé) le moins cher et la moyenne, ainsi que l'unité utilisé pour le prix (€/kg, €/litre, €/unité)

#2 dico données
* utilisateur
* créer
* produit
* liste de course
* lisser
* géolocalisation
* unité
* prix
* quantité
* catégorie
* magasin


#3 entités et compositions
* utilisateur
    * id
    * login
    * mdp
    * email?
* produit
    * id
    * libellé
* magasin
    * id
    * nom
    * position
* liste de course
    * id
    * libellé
* catégorie
    * id
    * libellé
