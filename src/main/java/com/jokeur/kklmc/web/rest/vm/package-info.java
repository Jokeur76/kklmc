/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jokeur.kklmc.web.rest.vm;
